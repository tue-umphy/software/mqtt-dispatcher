# MQTT-Dispatcher

Live data visualisation via Matplotlib

[![pipeline status](https://gitlab.com/tue-umphy/software/mqtt-dispatcher/badges/main/pipeline.svg)](https://gitlab.com/tue-umphy/software/mqtt-dispatcher/-/pipelines)
[![coverage report](https://gitlab.com/tue-umphy/software/mqtt-dispatcher/badges/main/coverage.svg)](https://tue-umphy.gitlab.io/software/mqtt-dispatcher/coverage-report/)
[![documentation](https://img.shields.io/badge/docs-sphinx-brightgreen.svg)](https://tue-umphy.gitlab.io/software/mqtt-dispatcher/)
[![PyPI](https://badge.fury.io/py/mqtt-dispatcher.svg)](https://badge.fury.io/py/mqtt-dispatcher)

`mqtt-dispatcher` is a software to do certain things if specific MQTT traffic happens.

## ✨ What can MQTT Dispatcher do?

MQTT Dispatcher connects to one or more MQTT brokers and monitors configured
traffic. If the traffic matches certain conditions, configured actions are
performed.

### Conditions

MQTT Dispatcher can check for the following conditions:

- check result of arbitrary Python code
- regular expression match agains topic and string-decoded message
- traffic within certain period of time
- no traffic within certain period of time

### Actions

MQTT Dispatcher can perform the following actions:

- send emails
- run arbitrary shell commands
- run arbitrary Python code

## 📥 Installation

The `mqtt-dispatcher` package is best installed via `pip`:

```bash
python3 -m pip install --user git+https://gitlab.com/tue-umphy/software/mqtt-dispatcher.git
```

This downloads and installs the latest development version directly from GitLab.

You may also install `mqtt-dispatcher` from the repository root:

```bash
python3 -m pip install --user .
```

## Documentation

Documentation of the `mqtt-dispatcher` package can be found [here on
GitLab](https://tue-umphy.gitlab.io/software/mqtt-dispatcher/).

Also, the command-line help page `mqtt-dispatcher -h` (or `mqtt-dispatcher
SUBCOMMAND -h`) is your friend.
