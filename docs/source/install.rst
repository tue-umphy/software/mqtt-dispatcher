Installation
============

:mod:`mqtt_dispatcher` is best installed directly from GitLab:

.. code-block:: sh

    python3 -m pip install --user git+https://gitlab.com/tue-umphy/software/mqtt-dispatcher

Depending on your setup it might be necessary to install the :mod:`pip` module
first:

.. code-block:: sh

    # Debian/Ubuntu
    sudo apt-get install python3-pip
    # Arch/Manjaro
    sudo pacman -Syu python-pip

Or see `Installing PIP`_.
