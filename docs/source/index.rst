Welcome to mqtt-dispatcher's documentation!
===========================================

:mod:`mqtt_dispatcher` is a Python package that performs predefined actions if
certain preconfigured conditions on MQTT traffic are met.

Functioning Principle
---------------------

This abstract graph shows how :mod:`mqtt_dispatcher` operates:

.. mermaid::

    graph TD
        broker(MQTT Broker Client)
        
        subgraph Matchers
        matcher1(Matcher 1)
        matcher2(Matcher 2)
        end

        subgraph Conditions
        condition1(Condition 1)
        condition2(Condition 2)
        condition3(Condition 3)
        end

        subgraph Actions
        action1(Action 1)
        action2(Action 2)
        action3(Action 3)
        end

        broker -->|incoming MQTT message| matcher1
        broker -->|incoming MQTT message| matcher2

        matcher1 -->|checks| condition1
        matcher2 -->|checks| condition3

        condition1 -->|met? then check| condition2
        condition2 -->|also met? then execute| action1
        condition3 -->|met? then execute| action2
        action2    -->|also execute| action3


Example
-------

Consider you have an MQTT broker in your smart home and you want to recieve an
email if someone rings the doorbell and you also want to get notified if no
traffic has been coming on relevant topics for a certain amount of time.  Also,
you have a temperature sensor in the basement and want to get notified if it's
too cold there.

This is how it would be set up in :mod:`mqtt_dispatcher`:

.. mermaid::


    graph TD
        broker(Your MQTT Broker)
        
        subgraph Matchers
        matcher1(No-traffic Alarm)
        matcher2(Doorbell Notification)
        matcher3(Basement too cold)
        end

        subgraph Conditions
        condition2(No traffic for a longer period?)
        condition3(Message on doorbell topic?)
        condition4(Message on temperature topic?)
        condition5(Temperature low?)
        end

        subgraph Actions
        action1(Send Email)
        end

        broker -->|incoming MQTT message| matcher1
        broker -->|incoming MQTT message| matcher2
        broker -->|incoming MQTT message| matcher3

        matcher1 -->|checks| condition2
        matcher2 -->|checks| condition3
        matcher3 -->|checks| condition4

        condition2 -->|also met? then execute| action1
        condition3 -->|met? then execute| action1
        condition4 -->|met? then check| condition5
        condition5 -->|also met? then execute| action1


Have a look at :doc:`cli` and :doc:`config` for how to set this up.
    
    
    

    
    

    
    
    

    
    

    
    
    



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   cli
   config
   changelog
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
