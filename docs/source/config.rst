Configuration
=============

In an :mod:`mqtt_dispatcher` configuration file you will want to have at least four sections:

.. code-block:: ini

   [broker:My Broker]
   # attach the matcher(s) to the broker/client
   matchers = My Matcher

   [matcher:My Matcher]
   # specify what conditions should be met for this matcher to hit
   conditions = My Condition
   # what actions should be executed if all conditions are met
   actions = My Action

   [condition:My Condition]
   # what comes in here is implementation-specific
   ...

   [action:My Action]
   # what comes in here is implementation-specific
   ...



Example
-------

Let's expand the example from the homepage :doc:`index`: You have an MQTT broker in
your smart home and you want to recieve an email if someone rings the doorbell
You also want to get notified if no traffic has been coming in for a certain
amount of time. Also, you have a temperature sensor in the basement and want to
get notified if it's too cold there.

This is how it would be set up in :mod:`mqtt_dispatcher`:

.. mermaid::


    graph TD
        broker(Your MQTT Broker)
        
        subgraph Matchers
        matcher1(No-traffic Alarm)
        matcher2(Doorbell Notification)
        matcher3(Basement too cold)
        end

        subgraph Conditions
        condition2(No traffic for a longer period?)
        condition3(Message on doorbell topic?)
        condition4(Message on temperature topic?)
        condition5(Temperature low?)
        end

        subgraph Actions
        action1(Send Email)
        end

        broker -->|incoming MQTT message| matcher1
        broker -->|incoming MQTT message| matcher2
        broker -->|incoming MQTT message| matcher3

        matcher1 -->|checks| condition2
        matcher2 -->|checks| condition3
        matcher3 -->|checks| condition4

        condition2 -->|also met? then execute| action1
        condition3 -->|met? then execute| action1
        condition4 -->|met? then check| condition5
        condition5 -->|also met? then execute| action1



The corresponding configuration ``house.conf`` would look like this:

.. code-block:: ini

    # this section specifies how to connect to your MQTT broker and what to do with
    # incoming traffic
    [broker:house]
    # defaults to localhost:1883
    host = url-of-your-broker.com
    port = 1883
    # what topics to subscribe to - here we subscribe to everything ('#')
    subscribe = #
    # these matchers (defined below) should be used to process incoming MQTT messages
    matchers = 
       doorbell
       silence
       basement-cold

    # lower-case keys are options to the section object. Options that are not
    # relevant to the section object are handled as „environment” variables which
    # are passed down in the chain of the graph above to matchers, conditions and
    # actions so they can be accessed there
    BROKER_NAME = My Home Broker


    # the following specifies the email sending action
    # MQTT Dispatcher determines what action you mean by looking at
    # the keys you provide here. Alternatively, you could also specify 
    # type = name-of-the-action-entry-point
    # to be explicit.
    [action:send-mail]
    host = smtp.yourmailprovider.com
    login = your-login-name
    # the following is for STARTTLS, use port=465 and use-tls=false (or leave it out) for SSL
    port = 587
    use-tls = ✔

    # all of the following are again „environment variables” which can be
    # overridden from the broker, matcher or condition.

    # If you don't like putting the password here in plain-text, you can also set
    # password-envvar = EMAIL_PASSWORD_VARIABLE_NAME
    # which will then fetch the password from the shell environment variable
    # EMAIL_PASSWORD_VARIABLE_NAME which you can set from outside mqtt-dispatcher however you like
    # e.g. by running 'read -s EMAIL_PASSWORD_VARIABLE_NAME && export EMAIL_PASSWORD_VARIABLE_NAME' 
    # before launching mqtt-dispatcher. This way, no password is stored in any file.
    password = ***********

    # The following are defaults. You can override them from broker, matcher or
    # condition sections to fine-tune the action.
    EMAIL_SENDER = you@yourmailprovider.com
    EMAIL_RECIPIENTS = yann.buechau@posteo.de
    EMAIL_SUBJECT = 📨 MQTT Message
    # This will appear in the Email body. You can use curly brackets to expand
    # „environment variables” that were set from broker, matchers or conditions.
    EMAIL_TEXT = 
        An MQTT message was recieved on broker {BROKER_HOST}:

        {TOPIC} = {MESSAGE}

    # this condition matches messages that contain dingdong (somewhere) on topic house/doorbell
    # by using regular expressions. Again, MQTT Dispatcher determines that you mean
    # a RegexCondition by looking at the keys you provide here.
    [condition:doorbell-message]
    topic = ^house/doorbell$
    message = dingdong

    # this conditions matches numeric messages that arrive at the house/basement/temperature topic
    [condition:basement-temperature]
    topic = ^house/basement/temperature$
    # this regular expression captures a digit and a unit in the variable TEMPERATURE
    # (look up „captured named groups in Python regular expressions” for details on this)
    message = ^(?P<TEMPERATURE>[\d.]+)\s+(?P<UNIT>.*)$

    # In this condition Python code is used to check if the temperature is low.
    # the TEMPERATURE variable was inherited from the previous condition
    # basement-temperature because the matcher executes them in order and
    # passes the variables on downwards.
    [condition:temperature-low]
    # If you have multiple lines of Python code, you have to indent them with a pipe symbol as below.
    # (and of course you have to adhere to the Python coding syntax)
    code =  | if float(TEMPERATURE) < 3 * math.pi:
            |     ENVIRONMENT["EMAIL_SUBJECT"] = (
            |         "Temperature is smaller than 3𝝿 {UNIT}: "
            |         "({diff} {UNIT} to be exact...)"
            |     ).format(
            |         UNIT=UNIT,
            |         TEMPERATURE=TEMPERATURE,
            |         diff=3 * math.pi - float(TEMPERATURE),
            |     )
            |     ret = True
            | else:
            |     ret = False
            | ret
    # ☝ The LAST Python code expression has to evaluate to True or False. 'return' statements don't work.

    # If your code needs imports, specify them here (one per line)
    # You can also specify 'math:pi', so only 'pi' is available.
    # You can do this with arbitrary importable modules 
    # (so for Python FILES make sure their directory is in the PYTHONPATH shell environment variable)
    imports = math

    # this condition is met if there has been no traffic for at least the given amount of seconds
    [condition:silence-for-5-min]
    # 5min * 60s/min = 300s
    silence-for = 300

    [match:doorbell]
    conditions = 
        doorbell-message
    actions = 
        send-mail
    # wait at least this many seconds until executing the action again (even if the conditions are met)
    cooldown = 3600

    # overwrite email subject and text to match the conditions here
    EMAIL_SUBJECT = 🛎 Somebody rung the doorbell! 🚪 🔔
    EMAIL_TEXT =
        Somebody rung your doorbell!
        Quick quick, go and open up!!

    [match:silence]
    conditions = 
        silence-for-5-min
    actions = 
        send-mail
    # wait at least this many seconds until executing the action again (even if the conditions are met)
    cooldown = 3600

    # overwrite email subject and text to match the conditions here
    EMAIL_SUBJECT = 😴 No traffic for a long time now...
    EMAIL_TEXT =
        Uh-oh, there hasn't been any traffic for a longer time now... 😟
        Maybe have a look at the network.

    [match:basement-cold]
    conditions = 
        basement-temperature
        temperature-low
    actions = 
        send-mail
    # wait at least this many seconds until executing the action again (even if the conditions are met)
    cooldown = 3600

    # overwrite email subject and text to match the conditions here
    EMAIL_SUBJECT = ❄ The Basement is cold! ⚠
    EMAIL_TEXT =
        The basement is cold! ({TEMPERATURE} {UNIT})
        Go check if everything is fine!


You then run :mod:`mqtt_dispatcher` like this (see also :doc:`cli`):

.. code-block:: sh

   mqtt-dispatcher --no-config config -r house.conf watch
