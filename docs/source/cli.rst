Command-Line Interface
======================


The basic usage of :mod:`mqtt_dispatcher` is like this:

.. code-block:: sh

   # read default config files (`.mqtt-dispatcher.conf` and 
   # `$XDG_CONFIG_HOME/.config/mqtt-dispatcher/mqtt-dispatcher.conf`),
   # then watch MQTT traffic and perform actions as configured 
   mqtt-dispatcher watch

   # same, but also read another given file
   mqtt-dispatcher config -r /path/to/config/file.conf watch

   # same, but without reading default files
   mqtt-dispatcher --no-config config -r /path/to/config/file.conf watch


Read on in :doc:`config` for how to configure the behaviour.

Debugging
---------

If you don't understand what's going on, you can add one or more ``-v`` flags
to increase the debug level:

.. code-block:: sh

   # insanely much debug output
   mqtt-dispatcher -vvvvvvvvv watch
   # really only errors
   mqtt-dispatcher -qqqqqqqqq watch
