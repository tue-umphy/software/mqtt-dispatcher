# system modules
import configparser
import collections
import logging
import os
import re

# internal modules
import mqtt_dispatcher
from mqtt_dispatcher.extensions import EntryPointExtensions
from mqtt_dispatcher.version import (
    CONDITION_ENTRY_POINT,
    ACTION_ENTRY_POINT,
    CONFIGURABLE,
)
from mqtt_dispatcher.client import Broker
from mqtt_dispatcher.action import Action
from mqtt_dispatcher.condition import Condition
from mqtt_dispatcher.match import Matcher

# external modules
import xdgspec

USER_CONFIG_FILE = os.path.join(
    xdgspec.XDGPackageDirectory("XDG_CONFIG_HOME", "mqtt-dispatcher").path,
    "mqtt-dispatcher.conf",
)
LOCAL_CONFIG_FILE = ".mqtt-dispatcher.conf"

DEFAULT_CONFIG_FILES = (USER_CONFIG_FILE, LOCAL_CONFIG_FILE)

logger = logging.getLogger(__name__)


class ActionSectionError(ValueError):
    pass


class Configuration(configparser.ConfigParser):
    """
    Class for configurations.
    """

    BROKER_SECTION_PREFIX = "broker"
    """
    Prefix for sections specifying a broker
    """

    ACTION_SECTION_PREFIX = "action"
    """
    Prefix for sections specifying an action
    """

    CONDITION_SECTION_PREFIX = "condition"
    """
    Prefix for sections specifying a condition
    """

    MATCH_SECTION_PREFIX = "match"
    """
    Prefix for sections specifying a match
    """

    def __init__(self, *args, **kwargs):
        configparser.ConfigParser.__init__(
            self, *args, **{**dict(interpolation=None), **kwargs}
        )

    @property
    def broker_section(self):
        """
        Generator yielding broker sections

        Yields:
            configparser.SectionProxy: the next broker section
        """
        for name, section in self.items():
            if name.startswith(self.BROKER_SECTION_PREFIX):
                yield section

    @property
    def action_section(self):
        """
        Generator yielding action sections

        Yields:
            configparser.SectionProxy: the next action section
        """
        for name, section in self.items():
            if name.startswith(self.ACTION_SECTION_PREFIX):
                yield section

    @property
    def condition_section(self):
        """
        Generator yielding condition sections

        Yields:
            configparser.SectionProxy: the next condition section
        """
        for name, section in self.items():
            if name.startswith(self.CONDITION_SECTION_PREFIX):
                yield section

    @property
    def match_section(self):
        """
        Generator yielding match sections

        Yields:
            configparser.SectionProxy: the next match section
        """
        for name, section in self.items():
            if name.startswith(self.MATCH_SECTION_PREFIX):
                yield section

    @property
    def brokers(self):
        """
        Yield :any:`Broker` s from the configuration
        """
        for section in self.broker_section:
            broker = Broker.from_config_section(section)
            broker.name = re.sub(
                f"{re.escape(self.BROKER_SECTION_PREFIX)}" r"\W+",
                "",
                section.name,
            )
            yield broker

    @property
    def actions(self):
        """
        Yield :any:`Action` s from the configuration
        """
        for section in self.action_section:
            action = Action.from_config_section(section)
            action.name = re.sub(
                f"{re.escape(self.ACTION_SECTION_PREFIX)}" r"\W+",
                "",
                section.name,
            )
            yield action

    @property
    def conditions(self):
        """
        Yield :any:`Condition` s from the configuration
        """
        for section in self.condition_section:
            broker = Condition.from_config_section(section)
            broker.name = re.sub(
                f"{re.escape(self.CONDITION_SECTION_PREFIX)}" r"\W+",
                "",
                section.name,
            )
            yield broker

    @property
    def matchers(self):
        """
        Yield :any:`Matcher` s from the configuration
        """
        for section in self.match_section:
            matcher = Matcher.from_config_section(section)
            matcher.name = re.sub(
                f"{re.escape(self.MATCH_SECTION_PREFIX)}" r"\W+",
                "",
                section.name,
            )
            yield matcher

    def optionxform(self, key):
        """
        Override to keep case in config keys
        """
        return key
