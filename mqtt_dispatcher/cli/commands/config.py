# system modules
import io
import logging

# internal modules
from mqtt_dispatcher.cli.commands.main import cli
from mqtt_dispatcher.config import DEFAULT_CONFIG_FILES

# external modules
import click

logger = logging.getLogger(__name__)


@cli.command(
    help="\n\n".join(
        (
            "managing configuration",
            (
                "The configuration files {files} are read by default "
                "if they exist "
                "unless the top-level --no-config option was specified."
            ).format(files=", ".join(map(repr, DEFAULT_CONFIG_FILES))),
            "Without options, this command just dumps the current "
            "configuration.",
        )
    ),
    short_help="managing configuration",
)
@click.option(
    "-r",
    "--read",
    help="read a configuration file",
    metavar="FILE",
    type=click.File("r"),
)
@click.option(
    "-s",
    "--save",
    help="save the current configuration to file",
    metavar="FILE",
    type=click.File("w"),
)
@click.pass_context
def config(ctx, read, save):
    if read:
        logger.info(
            "reading configuration file {file}".format(file=repr(read.name))
        )
        ctx.obj["config"].read_file(read)
    if save:
        logger.info(
            "saving current configuration to file {file}".format(
                file=repr(save.name)
            )
        )
        ctx.obj["config"].write(save)
    if not (read or save):
        buf = io.StringIO()
        ctx.obj["config"].write(buf)
        click.echo(buf.getvalue().strip())
