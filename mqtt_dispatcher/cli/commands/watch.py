# system modules
import logging
import re
import time

# internal modules
import mqtt_dispatcher
from mqtt_dispatcher.client import Broker
from mqtt_dispatcher.config import Configuration
from mqtt_dispatcher.cli.commands.main import cli

# external modules
import click
import attr

logger = logging.getLogger(__name__)


@cli.command(
    help="watch MQTT traffic and perform configured actions",
)
@click.option(
    "-i",
    "--reconnection-interval",
    help="seconds between reconnection attempts",
    metavar="SECONDS",
    show_default=True,
    default=30,
    type=click.FloatRange(min=0),
)
@click.pass_context
def watch(ctx, reconnection_interval):
    config = ctx.obj["config"]

    # turn configuration into objects
    actions = list(config.actions)
    conditions = list(config.conditions)
    matchers = list(config.matchers)
    brokers = list(config.brokers)

    def find_action(name):
        if not (action := next((a for a in actions if a.name == name), None)):
            raise click.ClickException(f"No configured action {repr(name)}")
        return action

    def find_condition(name):
        if not (
            condition := next((c for c in conditions if c.name == name), None)
        ):
            raise click.ClickException(f"No configured condition {repr(name)}")
        return condition

    def find_matcher(name):
        if not (
            matcher := next((m for m in matchers if m.name == name), None)
        ):
            raise click.ClickException(f"No configured matcher {repr(name)}")
        return matcher

    # connect the objects by translating the names to objects
    for matcher in matchers:
        matcher.actions = [find_action(n) for n in matcher.actions]
        matcher.conditions = [find_condition(n) for n in matcher.conditions]
    for broker in brokers:
        broker.matchers = [find_matcher(n) for n in broker.matchers]

    logger.info("Connecting to all globally configured brokers")
    for broker in brokers:
        logger.info(
            f"Starting connection to broker "
            f"{broker.host}:{broker.port} in the background..."
        )
        broker.client.loop_start()

    while True:
        time.sleep(reconnection_interval)
        for broker in brokers:
            broker.ensure_connection()
