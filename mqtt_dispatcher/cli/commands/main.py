# system modules
import logging
import re

# internal modules
from mqtt_dispatcher import utils
from mqtt_dispatcher.config import Configuration, DEFAULT_CONFIG_FILES

# external modules
import click

logger = logging.getLogger(__name__)


@click.group(
    help="MQTT Dispatcher",
    context_settings={
        "help_option_names": ["-h", "--help"],
        "auto_envvar_prefix": "MQTT_DISPATCHER",
    },
    chain=True,
)
@click.option(
    "-q",
    "--quiet",
    "quietness",
    help="decrease the verbosity. " "Can be specified multiple times.",
    count=True,
)
@click.option(
    "-v",
    "--verbose",
    "verbosity",
    help="increase the verbosity. "
    "Specifying this option more than 2 times "
    "enables all  log messages. More than 3 times doesn't limit "
    "logging to only .",
    count=True,
)
@click.option(
    "--no-config",
    help="don't load any default configuration files",
    is_flag=True,
)
@click.version_option(help="show version and exit")
@click.pass_context
def cli(ctx, quietness, verbosity, no_config):
    # set up logging
    loglevel_choices = dict(
        enumerate(
            (
                logging.CRITICAL + 1,
                logging.CRITICAL,
                logging.WARNING,
                logging.INFO,
                logging.DEBUG,
                logging.DEBUG_CALL,
            ),
            -3,
        )
    )
    loglevel = loglevel_choices.get(
        min(
            max(loglevel_choices),
            max(min(loglevel_choices), verbosity - quietness),
        )
    )
    logging.basicConfig(
        level=loglevel,
        format=f"[%(asctime)s] %(levelname)-8s "
        f"{'(%(name)s)' if verbosity >= 3 else ''} %(message)s",
    )
    for n, l in logger.manager.loggerDict.items():
        if verbosity < 2 and not re.match(r"^mqtt_dispatcher", n):
            l.propagate = False
            if hasattr(l, "setLevel"):
                l.setLevel(logging.CRITICAL + 1)
    # set up config
    ctx.ensure_object(dict)
    ctx.obj["config"] = Configuration()
    if no_config:
        logger.debug("skip reading default configuration files")
    else:
        logger.debug(
            "read default configuration files {files}".format(
                files=", ".join(map(repr, DEFAULT_CONFIG_FILES))
            )
        )
        ctx.obj["config"].read(DEFAULT_CONFIG_FILES)
