# system modules

# internal modules
from mqtt_dispatcher.cli.commands.main import cli

# external modules

if __name__ == "__main__":
    cli()
