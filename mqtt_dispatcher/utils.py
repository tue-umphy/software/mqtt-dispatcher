# system modules
import textwrap
import importlib
import math
import logging
import itertools
import functools
import collections
import re
from abc import ABC, abstractmethod

# internal modules
from mqtt_dispatcher.version import CONFIGURABLE, PUT_INTO_ENVIRONMENT_AS

# external modules
import attr
import asteval


logger = logging.getLogger(__name__)


@attr.s
class Environment:
    name = attr.ib(converter=str, default=None)
    environment = attr.ib(factory=dict, converter=dict)

    @classmethod
    def from_dict(cls, d):
        configurable_fields = set(
            f.name for f in attr.fields(cls) if f.metadata.get(CONFIGURABLE)
        )
        # pick out settable attributes
        kwargs = {
            k: d[k] for k in set(d).intersection(set(configurable_fields))
        }
        # make sure environment value is a dict
        kwargs["environment"] = dict(kwargs.get("environment", {}))
        obj = cls(**kwargs)
        # put attributes into environment
        for field in attr.fields(cls):
            if field.name == "environment":
                continue
            if field.metadata.get(PUT_INTO_ENVIRONMENT_AS) is not False:
                obj.environment[
                    field.metadata.get(PUT_INTO_ENVIRONMENT_AS, field.name)
                ] = getattr(obj, field.name)
        # rest of values ends up in the environment
        obj.environment.update(
            {
                k: d[k]
                for k in (set(d) - set(configurable_fields))
                if k != "environment"
            }
        )
        return obj

    @classmethod
    def all_subclasses(cls, given_cls=None):
        if given_cls is None:
            given_cls = cls
        for subcls in given_cls.__subclasses__():
            yield subcls
            for subsubcls in cls.all_subclasses(subcls):
                yield subsubcls

    @classmethod
    def from_config_section(cls, section):
        # dirty quickfix for circular import
        from mqtt_dispatcher.extensions import EntryPointExtensions

        if cls_type := section.get("type"):
            if not (
                matching_cls := next(
                    (
                        v
                        for k, v in EntryPointExtensions.get(
                            subcls=cls, aliases=True
                        ).items()
                        if cls_type in k
                    ),
                    None,
                )
            ):
                raise ValueError(
                    f"Unknown type {cls_type} " f"in section {section.name}"
                )
        else:
            if not any(
                (
                    attr_matches := {
                        c: set(
                            re.sub(r"\W+", "_", s) for s in section
                        ).intersection(
                            set(
                                f.name
                                for f in attr.fields(c)
                                if f.metadata.get(CONFIGURABLE)
                            )
                        )
                        for c in itertools.chain((cls,), cls.all_subclasses())
                    }
                ).values()
            ):
                raise ValueError(
                    f"No matching {cls} subclass with "
                    f"params {set(section)}: {attr_matches}"
                )
            attr_match_count = collections.defaultdict(list)
            for envcls, fields in attr_matches.items():
                attr_match_count[len(fields)].append(envcls)
            best_cls_matches = attr_match_count[max(attr_match_count)]
            if len(best_cls_matches) > 1:
                raise ValueError(
                    f"section {section.name} could specify "
                    f"{len(best_cls_matches)} ambiguous classes: "
                    f"{best_cls_matches} ({attr_matches})"
                )
            matching_cls = best_cls_matches[0]
        # instantiate the cls
        kwargs = {re.sub(r"\W+", "_", k): v for k, v in section.items()}
        kwargs.pop("type", None)
        try:
            obj = matching_cls.from_dict(kwargs)
        except BaseException as e:
            raise ValueError(
                f"Can't initialize {matching_cls.__name__} "
                f"from section {section.name}: {type(e)}: {e}"
            )
        return obj

    def __str__(self):
        return f"{type(self).__name__} {self.name!r}"


@attr.s
class MessageHandler(ABC, Environment):
    @abstractmethod
    def __call__(self, message, environment):
        pass


def split_string_by_newlines(x):
    if x is None:
        return x
    if isinstance(x, str):
        return list(
            s
            for s in re.split(pattern=r"\s*([\r\n]+\s*)+\s*", string=str(x))
            if not re.compile(r"\s+").fullmatch(s) and s
        )
    else:
        return list(map(str, x))


def split_string_by_whitespace(x):
    if x is None:
        return x
    if isinstance(x, str):
        return list(
            filter(
                bool,
                (
                    s
                    for s in re.split(pattern=r"\s+", string=str(x))
                    if not re.compile(r"\s+").fullmatch(s) and s
                ),
            )
        )
    else:
        return list(map(str, x))


def str2bool(s):
    return s.lower() in {"yes", "y", "true", "👍", "✅", "☑", "✔"}


def value_to_python_code(s):
    if m := re.match(r"(?P<indent>\|\s*)", s.strip()):
        s = "\n".join(
            re.sub(f"^\\s*{re.escape(m.groupdict()['indent'])}", r"", L)
            for L in re.split(r"[\r\n]+", s.strip())
        )
    s = textwrap.dedent(s.strip())
    return s


def lines_to_dict(x, sep=r":"):
    if not isinstance(x, str):
        return dict(x)
    return {
        next(e, None): next(e, None)
        for e in map(
            iter,
            (
                re.split(sep, s, maxsplit=1)
                for s in re.split(
                    pattern=r"\s*([\r\n]+\s*)+\s*", string=str(x)
                )
                if not re.compile(r"\s+").fullmatch(s) and s
            ),
        )
    }


@attr.s
class CodeExecutor:
    code = attr.ib(
        converter=value_to_python_code,
        default="True",
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "CODE",
        },
    )
    imports = attr.ib(
        converter=lines_to_dict,
        factory=list,
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "IMPORTS",
        },
    )

    def make_interpreter(self):
        interpreter = asteval.Interpreter()
        # add imports to symtable
        for module, element in self.imports.items():
            try:
                mod = importlib.import_module(module)
            except ImportError as e:
                logger.error(f"Can't import module {module}: {e}")
                continue
            if element is None:
                interpreter.symtable[module] = mod
            else:
                try:
                    obj = getattr(mod, element)
                except AttributeError:
                    logger.error(f"Module {module} does not contain {element}")
                    continue
                interpreter.symtable[element] = obj
        return interpreter

    def __call__(self, message, environment):
        interpreter = self.make_interpreter()
        symbols_after_init = set(asteval.Interpreter().symtable)
        # update symtable with environment
        environment = {
            **self.environment,
            **environment,
            **dict(
                TOPIC=message.topic,
                PAYLOAD=message.payload.decode(errors="coerce"),
                MESSAGE=message.payload.decode(errors="coerce"),
                QOS=message.qos,
                message=message,
            ),
        }
        interpreter.symtable.update(environment)
        interpreter.symtable["environment"] = environment
        interpreter.symtable["ENVIRONMENT"] = environment
        result = interpreter(self.code)
        if interpreter.error:
            for error in interpreter.error:
                logger.error(
                    f"{self.name}: error executing {repr(self.code)}: "
                    f"{error.get_error()}"
                )
            return None
        environment_updates = {
            k: v
            for k, v in interpreter.symtable.items()
            if k
            in (
                (set(interpreter.symtable) - symbols_after_init)
                - set(("ENVIRONMENT", "environment"))
            )
        }
        environment_updates.update(interpreter.symtable["environment"])
        self.environment.update(environment_updates)
        return result


# add a loglevel DEBUG_CALL
logging.DEBUG_CALL = logging.DEBUG - 1
logging.addLevelName(logging.DEBUG_CALL, "DEBUG_CALL")
logging.Logger.debug_call = (
    lambda self, message, *args, **kwargs: self._log(
        logging.DEBUG_CALL, message, args, **kwargs
    )
    if self.isEnabledFor(logging.DEBUG_CALL)
    else None
)


def debug(before=True, return_value=True):
    def decorator(decorated_fun):
        @functools.wraps(decorated_fun)
        def wrapper(*args, **kwargs):
            callstr = "{fun}({args})".format(
                fun=decorated_fun,
                args=", ".join(
                    map(
                        str,
                        itertools.chain(
                            args, (f"{k}={v}" for k, v in kwargs.items())
                        ),
                    )
                ),
            )
            if before:
                logger.debug_call(callstr)
            ret = decorated_fun(*args, **kwargs)
            if return_value:
                logger.debug_call(f"{callstr} returned {ret}")
            return ret

        return wrapper

    return decorator


def find_indentation(s):
    """
    Determine the indentation level of a string. Actually, :mod:`textwrap`
    should provide something like this and probably has it under the hood,
    but here it is...

    Args:
        s (str): the string to analyze

    Returns:
        str : the indentation whitespace

    Taken from PARMESAN:
    https://gitlab.com/tue-umphy/software/parmesan/-/blob/8e56a40771f36d281d3001dbdfd5f312ad90fe87/parmesan/utils/string.py#L10
    """
    return "".join(
        next(iter(x))
        for x in map(
            set,
            zip(*re.findall(pattern=r"[\r\n]+(\s*)", string="\r\n" + s)),
        )
        if len(x) == 1
    )


def find_code_indentation(s):
    """
    Given a Python code string, determine the used indentation level.

    Args:
        s : the Python code string

    Returns:
        str : the used indent

    Raises:
        ValueError : if the indenting is inconsistent
    """
    # all real indents
    if indents := sorted(
        set(filter(bool, re.findall(r"[\r\n]+(\s*)", "\r\n" + s))), key=len
    ):
        indent, indents = indents[0], indents[1:]
        for s in indents:
            if indent * (n := math.ceil(len(s) / len(indent))) != s:
                raise ValueError(
                    f"Indent {repr(s)} can't be expressed "
                    f"by {n} indents {repr(indent)}"
                )
        return indent
    else:
        return ""
