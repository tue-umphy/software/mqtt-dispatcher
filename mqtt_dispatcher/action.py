# system modules
import logging
import itertools
import shlex
import subprocess
import datetime
import os
import smtplib
from email.mime.text import MIMEText

# internal modules
from mqtt_dispatcher import utils
from mqtt_dispatcher.version import CONFIGURABLE, PUT_INTO_ENVIRONMENT_AS

# external modules
import attr
import paho.mqtt

logger = logging.getLogger(__name__)


@attr.s
class Action(utils.MessageHandler):
    def format(self, key, environment=None):
        """
        Recursively format a key in a given environment. Default to the own
        environment.
        """
        if environment is None:
            environment = self.environment
        n = itertools.count()
        value = environment.get(key, "")
        value_last = ""
        while value != value_last:
            value_last = value
            if next(n) > 5:  # stop a max recursion level
                break
            try:
                value = value.format(**environment)
            except BaseException as e:
                if n == 0:
                    logger.error(
                        f"Can't format {key!r} = {value!r}: {type(e)} {e}"
                    )
                break
        return value


@attr.s
class ExternalCommandAction(Action):
    command = attr.ib(converter=str, default="", metadata={CONFIGURABLE: True})
    forward_only_environment_variables = attr.ib(
        converter=lambda x: set(x)
        if isinstance(x, (tuple, list, set))
        else str(x).split(),
        default=set(),
        metadata={CONFIGURABLE: True},
    )
    timeout = attr.ib(
        converter=float,
        default=None,
        metadata={CONFIGURABLE: True, PUT_INTO_ENVIRONMENT_AS: "TIMEOUT"},
    )

    @utils.debug(before=False)
    def __call__(self, message, environment=None):
        environment = {
            **self.environment,
            **(environment or dict()),
            **dict(
                TOPIC=message.topic,
                MESSAGE=message.payload.decode(errors="coerce"),
                PAYLOAD=message.payload.decode(errors="coerce"),
                RETAIN=str(message.retain),
                QOS=str(message.qos),
                TIMESTAMP=(
                    datetime.datetime.utcnow().replace(
                        tzinfo=datetime.timezone.utc
                    )
                    - datetime.timedelta(
                        seconds=paho.mqtt.client.time_func()
                        - message.timestamp
                    )
                ).strftime("%Y-%m-%d %H:%M:%S%z"),
            ),
        }
        # substitute
        environment = {k: self.format(k, environment) for k in environment}
        cmd = shlex.split(self.command)
        if not cmd:
            logger.warning(f"Empty command ({repr(self.command)}).")
        logger.info(f"Executing {repr(self.command)}")
        process_environment = {
            **{str(k): str(v) for k, v in environment.items()},
            # forward all the shell environment if no whitelist is given
            **(
                os.environ
                if not self.forward_only_environment_variables
                else {
                    k: v
                    for k, v in os.environ.items()
                    if k in self.forward_only_environment_variables
                }
            ),
        }
        logger.debug(f"{self.forward_only_environment_variables = !r}")
        logger.debug(f"{process_environment = }")
        logger.debug(f"{os.environ = }")
        process = subprocess.Popen(
            shlex.split(self.command),
            env=process_environment,
        )
        logger.debug(
            f"Waiting "
            f"{'forever' if self.timeout is None else str(self.timeout)+'s'} "
            f"for {repr(self.command)} to finish..."
        )
        try:
            returncode = process.wait(timeout=self.timeout)
        except subprocess.TimeoutExpired:
            logger.warning(
                f"Killing process {process.pid} "
                f"as it takes longer than {self.timeout:2f}s"
            )
            process.kill()
            return
        (logger.error if returncode else logger.debug)(
            f"Command {repr(self.command)} exited with code {returncode}"
        )


@attr.s
class PythonAction(Action, utils.CodeExecutor):
    def __call__(self, message, environment):
        utils.CodeExecutor.__call__(self, message, environment)


@attr.s
class EmailAction(Action):
    host = attr.ib(
        default=None,
        metadata={CONFIGURABLE: True, PUT_INTO_ENVIRONMENT_AS: "EMAIL_HOST"},
    )
    port = attr.ib(
        default=465,
        metadata={CONFIGURABLE: True, PUT_INTO_ENVIRONMENT_AS: "EMAIL_PORT"},
    )
    login = attr.ib(
        default=None,
        metadata={CONFIGURABLE: True, PUT_INTO_ENVIRONMENT_AS: "EMAIL_LOGIN"},
    )
    password = attr.ib(
        default=None,
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "EMAIL_PASSWORD",
        },
    )
    password_envvar = attr.ib(
        default=None,
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "EMAIL_PASSWORD_ENVVAR",
        },
    )
    sender = attr.ib(
        default=None,
        metadata={CONFIGURABLE: True, PUT_INTO_ENVIRONMENT_AS: "EMAIL_SENDER"},
    )
    recipients = attr.ib(
        default=None,
        converter=utils.split_string_by_whitespace,
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "EMAIL_RECIPIENTS",
        },
    )
    use_tls = attr.ib(
        converter=utils.str2bool,
        default=False,
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "EMAIL_USE_TLS",
        },
    )
    subject = attr.ib(
        default="Notification from MQTT-Dispatcher",
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "EMAIL_SUBJECT",
        },
    )
    text = attr.ib(
        default="This is an email notification from MQTT-Dispatcher!"
        " (https://gitlab.com/tue-umphy/software/mqtt-dispatcher)",
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "EMAIL_TEXT",
        },
    )

    @utils.debug(before=False)
    def __call__(self, message, environment):
        environment = {
            **self.environment,
            **(environment or dict()),
        }
        logger.debug(f"Inside EmailAction {self.name}: {environment = }")
        if environment.get("EMAIL_USE_TLS"):
            server = smtplib.SMTP(
                environment.get("EMAIL_HOST"), environment.get("EMAIL_PORT")
            )
            server.starttls()
        else:
            server = smtplib.SMTP_SSL(
                environment.get("EMAIL_HOST"), environment.get("EMAIL_PORT")
            )
        login = environment.get("EMAIL_LOGIN")
        if not login:
            logger.warning(
                "Empty email login. This will most likely not work."
            )
        password = environment.get("EMAIL_PASSWORD") or os.environ.get(
            environment.get("EMAIL_PASSWORD_ENVVAR"), ""
        )
        if not password:
            logger.warning(
                "Empty email password. This will most likely not work."
            )
        server.login(
            login,
            password,
        )

        msg = MIMEText(self.format("EMAIL_TEXT", environment))
        msg["Subject"] = self.format("EMAIL_SUBJECT", environment)
        msg["From"] = self.format("EMAIL_SENDER", environment) or self.format(
            "EMAIL_LOGIN", environment
        )
        recipients = environment.get("EMAIL_RECIPIENTS") or environment.get(
            "EMAIL_LOGIN", ""
        )
        msg["To"] = ",".join(
            utils.split_string_by_whitespace(recipients)
            if isinstance(recipients, str)
            else recipients
        )
        server.send_message(msg)
        logger.info(f"Action {self.name}: Email sent successfully")
        server.quit()
