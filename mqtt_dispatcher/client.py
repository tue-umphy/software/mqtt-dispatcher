# system modules
import re
import logging
import datetime
import functools

# internal modules
from mqtt_dispatcher import utils
from mqtt_dispatcher.version import CONFIGURABLE, PUT_INTO_ENVIRONMENT_AS

# external modules
import paho.mqtt.client as mqtt
import attr


logger = logging.getLogger(__name__)


class Client(mqtt.Client):
    @property
    def topics_to_subscribe(self):
        try:
            return self._topics_to_subscribe
        except AttributeError:
            self._topics_to_subscribe = set()
        return self._topics_to_subscribe

    @topics_to_subscribe.setter
    def topics_to_subscribe(self, value):
        self._topics_to_subscribe = set(value)

    @property
    def matchers(self):
        try:
            return self._matchers
        except AttributeError:
            self._matchers = list()
        return self._matchers

    @matchers.setter
    def matchers(self, value):
        self._matchers = list(value)

    @property
    def environment(self):
        try:
            return self._environment
        except AttributeError:
            self._environment = dict()
        return self._environment

    @environment.setter
    def environment(self, value):
        self._environment = value

    def on_connect(self, client, userdata, flags, rc):
        logger.info(f"Connected to broker {self._host}:{self._port}")
        logger.debug(
            f"Connection to broker {self._host}:{self._port} "
            f"(userdata: {userdata}, flags: {flags}, rc: {rc})"
        )
        logger.info(
            f"Subscribing to topic patterns {repr(self.topics_to_subscribe)} "
            f"on broker {client._host}:{client._port}..."
        )
        self.subscribe([(topic, 0) for topic in self.topics_to_subscribe])

    def on_disconnect(self, client, userdata, rc):
        logger.info(f"Disconnected from broker {self._host}:{self._port}")
        logger.debug(
            f"Disconnection from broker {self._host}:{self._port} "
            f"(userdata: {userdata}, rc: {rc})"
        )
        if rc:
            logger.warning(
                f"Disconnected unexpectedly from broker "
                f"{self._host}:{self._port} (rc={rc})"
            )

    def on_subscribe(self, client, userdata, mid, granted_qos):
        logger.info(
            f"Subscribed (probably to {self.topics_to_subscribe}) "
            f"on broker {self._host}:{self._port}"
        )
        logger.debug(
            f"Subscription to broker {self._host}:{self._port} "
            f"(userdata: {userdata}, mid: {mid}, granted_qos: {granted_qos})"
        )

    def on_log(self, client, userdata, level, buf):
        logger.debug(
            f"MQTT client (userdata: {userdata}, level={level}): {buf}"
        )

    def on_message(self, client, userdata, message):
        logger.debug(
            f"Recieved {'retained ' if message.retain else ''}message "
            f"with qos {message.qos} "
            f"on topic {repr(message.topic)} "
            f"with payload {repr(message.payload)} "
            f"from broker {self._host}:{self._port}"
        )
        for matcher in self.matchers:
            logger.debug(
                f"checking if matcher {matcher.name} matches "
                f"message {repr(message.topic)}: {repr(message.payload)}"
            )
            matcher(message=message, environment=self.environment)


@attr.s
class Broker(utils.Environment):
    """
    Class representing an MQTT broker
    """

    host = attr.ib(
        converter=str,
        default="localhost",
        metadata={CONFIGURABLE: True, PUT_INTO_ENVIRONMENT_AS: "BROKER_HOST"},
    )
    port = attr.ib(
        converter=int,
        default=1883,
        metadata={CONFIGURABLE: True, PUT_INTO_ENVIRONMENT_AS: "BROKER_PORT"},
    )
    subscribe = attr.ib(
        factory=list,
        converter=utils.split_string_by_newlines,
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "BROKER_SUBSCRIBED_TOPICS",
        },
    )
    matchers = attr.ib(
        factory=list,
        converter=utils.split_string_by_newlines,
        metadata={CONFIGURABLE: True},
    )

    @property
    def client(self):
        try:
            return self._client
        except AttributeError:
            self._client = Client()
            # CAUTION: topic to subscribe only set ONCE here! No updates!
            self._client.topics_to_subscribe = self.subscribe
            self._client.matchers = self.matchers
            self._client.environment = self.environment
            self._client.connect_async(host=self.host, port=self.port)
        return self._client

    def handle_virtual_empty_message(self):
        """
        Lets all :any:`matchers` handle an empty :any:`MQTTMessage`
        """
        for matcher in self.matchers:
            message = mqtt.MQTTMessage()
            message.timestamp = mqtt.time_func()
            logger.debug(f"Handing an empty message to matcher {matcher.name}")
            matcher(message=message, environment=self.environment.copy())

    def ensure_connection(self):
        """
        Make sure all the client is connected. Sets ``BROKER_CONNECTION_TIME``
        in :any:`environment` to a :any:`datetime.datetime` or ``None`` on
        failure.
        """
        if self.client.is_connected() and self.client._thread.is_alive():
            logger.debug(
                f"still connected to broker "
                f"{self.client._host}:{self.client._port}"
            )
            self.environment.setdefault(
                "BROKER_CONNECTION_TIME",
                datetime.datetime.utcnow().replace(
                    tzinfo=datetime.timezone.utc
                ),
            )
            self.handle_virtual_empty_message()
        else:
            logger.info(
                f"Reconnecting to broker "
                f"{self.client._host}:{self.client._port}..."
            )
            self.environment["BROKER_CONNECTION_TIME"] = None
            try:
                if not self.client._thread.is_alive():
                    self.client.loop_start()
                if self.client.reconnect():
                    self.environment[
                        "BROKER_CONNECTION_TIME"
                    ] = datetime.datetime.utcnow().replace(
                        tzinfo=datetime.timezone.utc
                    )
                    self.handle_virtual_empty_message()
            except BaseException as e:
                logger.error(
                    f"Couldn't reconnect to broker "
                    f"{self.client._host}:{self.client._port}: {type(e)} : {e}"
                )
