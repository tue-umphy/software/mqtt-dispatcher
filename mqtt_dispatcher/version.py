__version__ = "0.0.0"

CONDITION_ENTRY_POINT = "mqtt_dispatcher.condition"

ACTION_ENTRY_POINT = "mqtt_dispatcher.action"

CONFIGURABLE = "configurable"

PUT_INTO_ENVIRONMENT_AS = "into-environment-as"
