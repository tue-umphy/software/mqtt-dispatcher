# system modules
import logging
import time

# internal modules
from mqtt_dispatcher import utils
from mqtt_dispatcher.version import CONFIGURABLE

# external modules
import attr

logger = logging.getLogger()


@attr.s
class Matcher(utils.MessageHandler):
    conditions = attr.ib(
        factory=list,
        converter=utils.split_string_by_newlines,
        metadata={CONFIGURABLE: True},
    )
    actions = attr.ib(
        factory=list,
        converter=utils.split_string_by_newlines,
        metadata={CONFIGURABLE: True},
    )
    cooldown = attr.ib(
        converter=float, default=0, metadata={CONFIGURABLE: True}
    )
    last_match_time = attr.ib(converter=float, default=0)

    @utils.debug(before=False)
    def __call__(self, message, environment):
        environment = {**self.environment, **environment}
        conditions_checked = 0
        for condition in self.conditions:
            result = condition(message=message, environment=environment)
            if result is None:
                logger.debug(
                    f"Matcher {self.name}: skip condition {condition.name}"
                )
                continue
            if result is False:
                return False
            environment.update(condition.environment)
            conditions_checked += 1
        if not conditions_checked:
            logger.debug(
                f"Matcher {self.name}: "
                f"all {len(self.conditions)} conditions skipped"
            )
            return False
        logger.debug(
            f"Matcher {self.name}: all {len(self.conditions)} "
            f"conditions are met for MQTT message "
            f"{repr(message.payload)} at topic {repr(message.topic)}"
        )
        time_since_last_match = time.time() - self.last_match_time
        if time_since_last_match < self.cooldown and self.last_match_time:
            logger.debug(
                f"Matcher {self.name}: "
                f"Conditions do match, but we're still within "
                f"{self.cooldown}-seconds cooldown time "
                f"({self.cooldown - time_since_last_match:.1f}s left)..."
            )
            return False
        self.last_match_time = time.time()
        logger.debug(
            f"Matcher {self.name}: "
            f"Executing actions for message {message.payload} "
            f"on topic {message.topic}"
        )
        for action in self.actions:
            action(message=message, environment=environment)
        return True
