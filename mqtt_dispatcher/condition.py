# system modules
import logging
import functools
import itertools
import re

# internal modules
from mqtt_dispatcher import utils
from mqtt_dispatcher.version import CONFIGURABLE, PUT_INTO_ENVIRONMENT_AS

# external modules
import paho.mqtt.client as mqtt
import attr


logger = logging.getLogger(__name__)


@attr.s
class Condition(utils.MessageHandler):
    @staticmethod
    def message_is_real(message):
        return all(
            (
                message.state == mqtt.mqtt_ms_invalid,
                message.topic,
            )
        )

    @staticmethod
    def only_real_traffic(decorated_fun):
        """
        Decorator for Condition.__call__() implementations to ignore empty
        messages
        """

        @functools.wraps(decorated_fun)
        def wrapper(*args, **kwargs):
            for message in (
                x
                for x in itertools.chain(args, kwargs.values())
                if isinstance(x, mqtt.MQTTMessage)
            ):
                if not Condition.message_is_real(message):
                    return None
            return decorated_fun(*args, **kwargs)

        return wrapper


@attr.s
class PythonCondition(Condition, utils.CodeExecutor):
    @utils.debug(before=False)
    @Condition.only_real_traffic
    def __call__(self, message, environment):
        return utils.CodeExecutor.__call__(self, message, environment)


def compile_regex(s):
    return re.compile(s, flags=re.IGNORECASE)


@attr.s
class RegexCondition(Condition):
    """
    Regex match against the MQTT topic and message
    """

    topic = attr.ib(
        converter=compile_regex,
        default=r"^.*$",
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "TOPIC_PATTERN",
        },
    )

    message = attr.ib(
        converter=compile_regex,
        default=r"^.*$",
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "MESSAGE_PATTERN",
        },
    )

    @utils.debug(before=False)
    @Condition.only_real_traffic
    def __call__(self, message, environment):
        if (m_topic := self.topic.search(message.topic)) and (
            m_message := self.message.search(
                message.payload.decode(errors="coerce")
            )
        ):
            self.environment.update(m_topic.groupdict())
            self.environment.update(m_message.groupdict())
            return True
        else:
            return False


@attr.s
class TrafficCondition(Condition):
    """
    Condition that is met if there is MQTT traffic within a certain time
    period.
    """

    last_time = attr.ib(converter=float, default=0)
    traffic_within = attr.ib(
        converter=float,
        default=0,
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "TRAFFIC_WITHIN",
        },
    )

    @utils.debug(before=False)
    @Condition.only_real_traffic
    def __call__(self, message, environment):
        last_time = self.last_time
        self.last_time = message.timestamp
        if last_time:
            if message.timestamp - last_time < self.traffic_within:
                return True
        else:
            return False


@attr.s
class SilenceCondition(Condition):
    """
    Condition that is met if there is no MQTT traffic for a period of time.
    """

    last_time = attr.ib(converter=float, default=0)
    silence_for = attr.ib(
        converter=float,
        default=0,
        metadata={
            CONFIGURABLE: True,
            PUT_INTO_ENVIRONMENT_AS: "SILENCE_FOR",
        },
    )

    @utils.debug(before=False)
    def __call__(self, message, environment):
        real_message = self.message_is_real(message)
        if real_message:
            logger.debug(
                f"{self.name}: okay, this is a real message, "
                f"resetting silence timer"
            )
            self.last_time = message.timestamp
            return False
        if not self.last_time:
            logger.debug(
                f"{self.name}: never recieved any message yet, "
                f"this is the first one, start counting now"
            )
            self.last_time = message.timestamp
            return False
        if message.timestamp - self.last_time > self.silence_for:
            logger.debug(
                f"{self.name}: okay, "
                f"last real message was at {self.last_time:.2f}, "
                f"which was {message.timestamp - self.last_time:.2f}s "
                f"(more than {self.silence_for} seconds) ago"
            )
            return True
        return False
