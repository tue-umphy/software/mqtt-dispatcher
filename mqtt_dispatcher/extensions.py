# system modules
import pkg_resources
import warnings
import logging

# internal modules
from mqtt_dispatcher.version import CONDITION_ENTRY_POINT, ACTION_ENTRY_POINT
from mqtt_dispatcher.condition import Condition
from mqtt_dispatcher.action import Action

# external modules

logger = logging.getLogger(__name__)


class EntryPointExtensions:
    """
    Class for detection of entry point extensions for :mod:`mqtt_dispatcher`
    """

    entry_point_subclasses = {
        CONDITION_ENTRY_POINT: Condition,
        ACTION_ENTRY_POINT: Action,
    }

    @classmethod
    def get(cls, name=None, subcls=None, aliases=False):
        """
        Check entry points. Invalid entry points are skipped with a warning.
        Entry points pointing to classes not inheriting from a given class are
        still included but with a warning.

        Args:
            name (str,optional): the name of the entry point
            subcls (class, optional): class to check inheritance for. Defaults
                to :class:`object`.
            aliases (bool, optional): Whether to pass the result through
                :meth:`aliases_from_entry_points` to determine the aliases.
                Default is ``False``.

        Returns:
            dict : mapping of :class:`pkg_resources.EntryPoint` s (if
            ``aliases`` is ``False``, tuples of unique alias strings otherwise)
            to the respective classes
        """
        if name is None and subcls is None:
            raise ValueError("specify at least name or subcls")
        if name is None:
            names = tuple(
                k
                for k, v in cls.entry_point_subclasses.items()
                if (issubclass(v, subcls) or issubclass(subcls, v))
            )
            if len(names) != 1:
                raise ValueError(
                    f"{len(names)} entry points match subcls={subcls}: {names}"
                )
            name = next(iter(names))
        if subcls is None:
            subcls = cls.entry_point_subclasses.get(name, object)
        entry_points = {}
        for entry_point in pkg_resources.iter_entry_points(name):
            try:
                entry_point_cls = entry_point.load()
            except BaseException as e:
                warnings.warn(
                    "Could not load {entry_point} entry point {name} "
                    "from {dist}: {err}".format(
                        entry_point=repr(name),
                        name=repr(entry_point.name),
                        err=e,
                        dist=repr(entry_point.dist or "unknown distribution"),
                    )
                )
                continue
            try:
                if not issubclass(entry_point_cls, subcls):
                    warnings.warn(
                        (
                            "{entry_point} entry point named {name} "
                            "from {dist} points to class "
                            "{entry_point_cls} which is not a "
                            "{subcls} subclass "
                            "and thus might not work"
                        ).format(
                            entry_point=repr(name),
                            name=repr(entry_point.name),
                            entry_point_cls=entry_point_cls,
                            subcls=subcls,
                            dist=repr(
                                entry_point.dist or "unknown distribution"
                            ),
                        )
                    )
            except BaseException as e:
                warnings.warn(
                    (
                        "Skipping {entry_point} entry point named {name} "
                        "from {dist} pointing to {obj} which is "
                        "obviously not a type: {err}"
                    ).format(
                        entry_point=repr(subcls),
                        name=repr(entry_point.name),
                        obj=repr(entry_point_cls),
                        err=e,
                        dist=repr(entry_point.dist or "unknown distribution"),
                    )
                )
                continue
            matching_ep = next(
                filter(lambda ep: ep.name == entry_point.name, entry_points),
                None,
            )
            if matching_ep:
                warnings.warn(
                    (
                        "{dist1} and {dist2} both define a {entry_point} "
                        "entry point {name}."
                    ).format(
                        dist1=repr(matching_ep.dist or "unknown distribution"),
                        dist2=repr(entry_point.dist or "unknown distribution"),
                        entry_point=repr(name),
                        name=repr(entry_point.name),
                    )
                )
            entry_points[entry_point] = entry_point_cls
        return (
            cls.aliases_from_entry_points(entry_points)
            if aliases
            else entry_points
        )

    @staticmethod
    def aliases_from_entry_points(entry_points):
        """
        Create mapping of aliases to classes from

        Args:
            entry_points (dict): mapping of entry points to the respective
                classes

        Returns:
            dict : mapping of tuples of unique alias strings to classes
        """
        final_aliases = {}
        for entry_point, final_cls in sorted(
            entry_points.items(),
            key=lambda x: (
                "0"
                if x[0].module_name.startswith("mqtt_dispatcher")
                else str(x[0])
            ),
        ):

            aliases = []
            full = (
                ".".join(filter(bool, (entry_point.module_name, a)))
                for a in entry_point.attrs
            )
            for alias in filter(
                bool,
                (entry_point.name,) + tuple(entry_point.attrs) + tuple(full),
            ):
                if any(alias in a for a in final_aliases):
                    continue
                aliases.append(alias)
            final_aliases[tuple(aliases)] = final_cls
        return final_aliases

    @staticmethod
    def short_aliases(aliases):
        """
        Generate a list of aliases

        Args:
            aliases (sequence of sequences): sequence of sequences with alias
                strings

        Returns:
            tuple : the first aliases
        """
        return tuple(
            map(
                repr,
                sorted(filter(bool, (next(iter(a), None) for a in aliases))),
            )
        )
